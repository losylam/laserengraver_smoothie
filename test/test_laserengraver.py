#!/env/python
#!coding: utf-8

import sys
import os

sys.path.append('lib/inkex-0.92')
sys.path.append('../src')

import laserengraver_smoothie

def get_simple_args(file_in, file_out, args_plus = {}):
    args_default = {'--add-numeric-suffix-to-filename': 'False',
                    '--header-verbose':'False'}
    args_lst = []

    for c, i in args_plus.items():
        args_lst.append(c)
        args_lst.append(i)

    for c, i in args_default.items():
        args_lst.append(c)
        args_lst.append(i)
        
    args_lst += ['-f', file_out, '-d', 'output', os.path.join('input', file_in)]

    print args_lst

    return args_lst

def in_out_test_default(file_in, file_out, args_plus = {}):
    """
    test a file output compared to a reference
    """
    effects_args = get_simple_args(file_in, file_out, args_plus)

    e = laserengraver_smoothie.Laserengraver()
    e.affect(args=effects_args, output=False)

    f_ref = open(os.path.join('output_ref', file_out), 'r') 
    f_out = open(os.path.join('output', file_out), 'r')
    return f_ref.read(), f_out.read()


## Test basic features
def test_carre_091():
    f_ref, f_out = in_out_test_default('carre_091.svg', 'carre.gcode')
    assert f_ref == f_out
    
def test_carre():
    f_ref, f_out = in_out_test_default('carre.svg', 'carre.gcode')
    assert f_ref == f_out

def test_carre_raster():
    f_ref, f_out = in_out_test_default('carre_raster.svg', 
                                       'carre_raster_091.gcode', 
                                       {'--img-toprint':'True'})
    assert f_ref == f_out
    
def test_carre_raster_091():
    f_ref, f_out = in_out_test_default('carre_raster_091.svg', 
                                       'carre_raster_091.gcode', 
                                       {'--img-toprint':'True'})
    assert f_ref == f_out

def test_carre_raster_decalage():
    f_ref, f_out = in_out_test_default('carre_raster_decalage.svg', 
                                       'carre_raster_decalage_091.gcode', 
                                       {'--img-toprint':'True'})
    assert f_ref == f_out
    
def test_carre_raster_decalage_091():
    f_ref, f_out = in_out_test_default('carre_raster_decalage_091.svg', 
                                       'carre_raster_decalage_091.gcode', 
                                       {'--img-toprint':'True'})
    assert f_ref == f_out

def test_carre_raster_translate():
    f_ref, f_out = in_out_test_default('carre_raster_translate.svg', 
                                       'carre_raster_translate.gcode', 
                                       {'--img-toprint':'True'})
    assert f_ref == f_out

def test_carre_raster_translate_091():
    f_ref, f_out = in_out_test_default('carre_raster_translate_091.svg', 
                                       'carre_raster_translate.gcode', 
                                       {'--img-toprint':'True'})
    assert f_ref == f_out

def test_raster_layer():
    """
    issue 5
    """
    f_ref, f_out = in_out_test_default('test_issue5_raster_layer.svg',
                                       'test_issue5_raster_layer_out.gcode',
                                       {'--img-toprint':'True'})
    assert f_ref == f_out
    
## Test bug related
def test_gcodeR():
    f_ref, f_out = in_out_test_default('test-gcodeR-0.svg', 'test-gcodeR-0-092.gcode')
    assert f_ref == f_out

def test_issue18():
    f_ref, f_out = in_out_test_default('test_issue18.svg', 'test_issue18_out.gcode')
    assert f_ref == f_out

def test_issue29():
    """
    translate raster
    """
    f_ref, f_out = in_out_test_default('test_issue29_bitmap_translate.svg',
                                       'test_issue29_bitmap_translate_out.gcode',
                                       {'--img-toprint':'True',
                                        '--img-min-power':'0'})
    assert f_ref == f_out
    
## Test complex files
def test_rond_carre_plateformec():
    f_ref, f_out = in_out_test_default('rond_carre_plateformec.svg',
                                       'rond_carre_plateformec.gcode', 
                                       {'--img-toprint':'True'})
    assert f_ref == f_out

## Test debug output

# trouver un moyen de capter la sortie standard, ou d'exporter le fichier! 
# def test_img_footprint():
#     effects_args = get_simple_args('test_img_footprint.svg',
#                                    'test_img_footprint_out.gcode',
#                                    {'--img-toprint':'True'})

#     e = laserengraver_smoothie.Laserengraver()
#     svg_out = e.affect(args=effects_args, output=True)

#     print 'svg_out'
#     print sys.stdout
    
#     with open('output_ref/test_img_footprint_out.svg', 'w') as f:
#         f.write(svg_out)
    
#     svg_ref = open(os.path.join('output_ref', 'test_img_footprint_out.svg'), 'r') 
#     f_out = open(os.path.join('output', file_out), 'r')
#     assert svg_ref.read() == svg_out
    
