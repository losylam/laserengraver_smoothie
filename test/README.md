Test architecture for laserengraver_smoothie
=====

An architecture to test laserengraver_smoothie without running inkscape
Python dependencies for inkex in ./lib


How to use virtualenv
----

Need python-virtualenv

installation from debian repo :
$ sudo apt-get install python-virtualenv

with pip
$ pip install --user virtualenv

run the script set_venv.sh to
- create venv
- install inkscape extension dependencies : lxml, numpy
- install pytest

How to perform tests
----
run run_test.sh