# Laserengraver Smoothie

Laserengraver_smoothie est une extension du logiciel [inkscape](inkscape.org) qui permet de 
convertir un dessin vectoriel en un fichier gcode adapté à une découpe laser 
équipée d'un carte de controle de type [Smoothie](smoothieware.org/).

Cette extension permet de réaliser trois types de choses :
- De la gravure selon un chemin 
- Du raster à partir d'images en niveaux de gris
- De la decoupe selon un chemin

Cette extension est basée sur :
- L'extension [gcodetools](https://github.com/cnc-club/gcodetools) d'inskape 
- Le script [Raster2gcode](http://fablabo.net/wiki/Raster2Gcode)

Cette extension est développée au sein du FabLab Nantais Plateforme-c 
(plateforme-c.org) grâce à certains de ses membres et permanents.

## Installation 

Copier les deux fichiers du dossier src (laserengraver_laser_smoothie.inx et laserengraver_smoothie.py) dans le dossier d'extensions d'inkscape.

## Documentation 

La documentation de laserengraver_smoothie est disponible sur le site [fablabo](http://fablabo.net/wiki/Laserengraver).

## Licence

L'ensemble des élements constitutants l'extension laserengraver sont sous 
licence GNU GPL.
